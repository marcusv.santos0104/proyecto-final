from cliente import Cliente


class Credito(Cliente):
    valor = 0

    def crearNuevo(self, nombre, apellido, edad, direccion, valor):
        self.nombre = nombre
        self.apellido = apellido
        self.edad = edad
        self.direccion = direccion
        self.valor = valor

    def consulta(self):
        print("Crédito atual: $", self.valor)

    def recargar(self, valor):
        self.valor = self.valor + valor
        print("--- Recargado con exito ---")
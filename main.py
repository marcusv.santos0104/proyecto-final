from cliente import Cliente
from credito import Credito

def menu():
    print("- Controle de Crédito -")
    print("1. Agregar Nuevo Cliente")
    print("2. Atualizar Cliente")
    print("3. Ver Cliente")
    print("4. Consultar Crédito")
    print("5. Recargar")
    print("6. Salir")
    opc = int( input(("\nOpcion: ")))
    return opc

if __name__ == '__main__':
    print("--- Sistema ---")

    x = 0

    while x != 1:
        opcion = menu()
        if opcion == 1:
            # Agregar Cliente
            nuevo_cliente = Credito()
            nombre = input("Nombre: ")
            apellido = input("Apellido: ")
            edad = input("Edad: ")
            direccion = input("Direccion: ")
            valor = int(input("Valor inicial: "))
            nuevo_cliente.crearNuevo(nombre, apellido, edad, direccion, valor)
        elif opcion == 2:
            # Atualizar Cliente
            nombre = input("Nombre: ")
            apellido = input("Apellido: ")
            edad = input("Edad: ")
            direccion = input("Direccion: ")
            nuevo_cliente.agregar(nombre, apellido, edad, direccion)
        elif opcion == 3:
            nuevo_cliente.ver()
        elif opcion == 4:
            nuevo_cliente.consulta()
        elif opcion == 5:
            valor = int(input("Valor da recarga: "))
            nuevo_cliente.recargar(valor)
        elif opcion == 6:
            print("¿Desea salir del sistema? (1 = Si    0 = No)")
            x = int(input(''))
        else:
            print("Inválida.")
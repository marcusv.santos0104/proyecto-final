import uuid
id_ramdom = 0


class Cliente:
    global id_ramdom
    id = 0
    nombre = ""
    apellido = ""
    edad = ""
    direccion = ""

    def __init__(self):
        self.id = str(id_ramdom + 1)

    def agregar(self, nombre, apellido, edad, direccion):
        self.nombre = nombre
        self.apellido = apellido
        self.edad = edad
        self.direccion = direccion

    def ver(self):
        print("ID: ", self.id)
        print("Nombre: ", self.nombre)
        print("Apellido: ", self.apellido)
        print("Edad: ", self.edad)
        print("Dirección:", self.direccion)

    def eliminar(self):
        self.id = ""
        self.nombre = ""
        self.apellido = ""
        self.edad = ""
        self.direccion = ""
